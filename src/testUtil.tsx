/**
 * A set of common utilities for testing
 */

// React
import React from 'react';

// Enzyme
import { ShallowWrapper } from 'enzyme';


/**
 * Class to assist wrapper configurations to be passed in as an object in functions,
 * it provides default values and typing to the config.
 *
 * @Constructor
 */
export class WrapperConfig {

  // Default values for each individual property
  public props: object = {};
  public initialState: object = {};
  public children: JSX.Element = <div/>;

  // Constructor params provides typing for all property,
  // as well as a default object if no wrapperConfig was not passed.
  // Note that the default object should match the default property.
  constructor( wrapperConfig = { props: {}, initialState: {}, children: <div/> } ) {
    this.props = wrapperConfig.props || this.props;
    this.initialState = wrapperConfig.initialState || this.initialState;
    this.children = wrapperConfig.children || this.children;
  }
}


/**
 * Return ShallowWrapper containing node(s) with give data-test-name value.
 *
 * @function findTestName
 * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
 * @param {string} value - value of the data-test-name attribute to search.
 * @returns {ShallowWrapper}
 */
export const findTestName = ( wrapper: ShallowWrapper, value: string ) => {
  const attributeKey = 'data-test-name';
  return wrapper.find( `[${attributeKey}='${value}']`);
};

// React
import { ConnectedRouter } from 'connected-react-router';
import React from 'react';

// Material UI
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

// Redux
import { Provider } from 'react-redux';
import store, { history } from './redux/store/store';

// Components
import Main from './ProjectComponents/Main/Main';

// Styles
import './App.scss';


const theme = createMuiTheme({

  palette: {
    primary: {
      main: '#57c7ff',
    },
  },

});


const App = () => {

  return (

    <Provider store={store}>
      <ConnectedRouter history={history}>
        <ThemeProvider theme={theme}>
          <Main />
        </ThemeProvider>
      </ConnectedRouter>
    </Provider>

  );

};


export default App;

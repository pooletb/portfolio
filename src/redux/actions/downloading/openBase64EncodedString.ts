export const openBase64EncodedString = (file: string, fileType: string, name: string) => {

  switch (fileType) {

    case 'PDF':
      downloadPDF(file, name);
      break;

      default:
        break;

  }

};


function downloadPDF(pdf: string, fileName: string) {

  const binary = atob(pdf.replace(/\s/g, ''));
  const len = binary.length;
  const buffer = new ArrayBuffer(len);
  const view = new Uint8Array(buffer);

  for (let i = 0; i < len; i++) {

      view[i] = binary.charCodeAt(i);

  }

  const downloadLink = document.createElement('a');
  downloadLink.style.display = 'none';
  const blob = new Blob( [view], { type: 'application/pdf' });
  const url = window.URL.createObjectURL(blob);
  downloadLink.href = url;
  downloadLink.download = fileName + '.pdf';
  document.body.appendChild(downloadLink);
  downloadLink.click();

  setTimeout(() => {
    window.close();
    window.location.href = '/';
  }, 100);

}

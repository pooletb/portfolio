// Redux
import { Dispatch } from 'redux';
import {
    hideLoading,
    setEducationEntries,
    setNewsEntries,
    setResume,
    setTechnicalSkillEntries,
    setTutorialEntries,
    setWorkExperienceEntries,
} from '../../action-creators/globalActionCreators';


export const fetchEntries =
    (type: 'NewsEntry' |
        'EducationEntry' |
        'TechnicalSkillEntry' |
        'WorkExperienceEntry' |
        'TutorialEntry' |
        'Resume') => {

    return async (dispatch: Dispatch<any>) => {

        try {

            const response = await fetch(`/api/portfolio?type=${type}`, {

                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },

            });

            const result = await response.json();

            if (response.status !== 200 || result.success === false) {

                throw new Error();

            }

            dispatch(hideLoading());

            switch (type) {

                case 'NewsEntry':
                    dispatch(setNewsEntries(result.detail));
                    break;

                case 'EducationEntry':
                    dispatch(setEducationEntries(result.detail));
                    break;

                case 'TechnicalSkillEntry':
                    dispatch(setTechnicalSkillEntries(result.detail));
                    break;

                case 'WorkExperienceEntry':
                    dispatch(setWorkExperienceEntries(result.detail));
                    break;

                case 'TutorialEntry':
                    dispatch(setTutorialEntries(result.detail));
                    break;

                case 'Resume':
                    dispatch(setResume(result.detail[0]));
                    break;

                default:
                    break;

            }

        } catch (err) {

            dispatch(hideLoading());

        }
    };

};


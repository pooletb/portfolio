import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';
import globalReducer from './globalReducer';


export default (history: any) => combineReducers({

  router: connectRouter(history),
  global: globalReducer,

});

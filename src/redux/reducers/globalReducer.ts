// Actions
import {

    SET_EDUCATION_ENTRIES,
    SET_ERROR,
    SET_LOADING,
    SET_NEWS_ENTRIES,
    SET_RESUME,
    SET_TECHNICAL_SKILL_ENTRIES,
    SET_TUTORIAL_ENTRIES,
    SET_WORK_EXPERIENCE_ENTRIES,

} from '../action-types/globalActionTypes';

// Classes
import GlobalReducerActions from '../../classes/GlobalReducerActions';
import GlobalReducerState from '../../classes/GlobalReducerState';


const initialState = new GlobalReducerState();


export function globalReducer(state = initialState, action: GlobalReducerActions) {

    switch (action.type) {

        case SET_ERROR:
            return {...state, errorMessage: action.errorMessage};

        case SET_LOADING:
            return {...state, isLoading: action.isLoading};

        case SET_NEWS_ENTRIES:
            return {...state, newsEntries: action.newsEntries};

        case SET_EDUCATION_ENTRIES:
            return {...state, educationEntries: action.educationEntries};

        case SET_TECHNICAL_SKILL_ENTRIES:
            return {...state, technicalSkillEntries: action.technicalSkillEntries};

        case SET_WORK_EXPERIENCE_ENTRIES:
            return {...state, workExperienceEntries: action.workExperienceEntries};

        case SET_TUTORIAL_ENTRIES:
            return {...state, tutorialEntries: action.tutorialEntries};

        case SET_RESUME:
            return {...state, resume: action.resume};

        default:
            return state;

    }

}


export default globalReducer;

import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from '../../../node_modules/redux';
import thunk from '../../../node_modules/redux-thunk';
import rootReducer from '../reducers/rootReducer';

export const history: any = createBrowserHistory();

const middleware = [

    thunk,
    routerMiddleware(history),

];

const initialState = {};

const composedEnhancers = compose(applyMiddleware(...middleware));

const store = createStore(rootReducer( history ), initialState, composedEnhancers);

export default store;

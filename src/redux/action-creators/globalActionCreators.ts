import {
  SET_EDUCATION_ENTRIES,
  SET_ERROR, SET_LOADING,
  SET_NEWS_ENTRIES,
  SET_RESUME,
  SET_TECHNICAL_SKILL_ENTRIES,
  SET_TUTORIAL_ENTRIES,
  SET_WORK_EXPERIENCE_ENTRIES,
} from '../action-types/globalActionTypes';

// Classes
import EducationEntry from '../../classes/EducationEntry';
import NewsEntry from '../../classes/NewsEntry';
import Resume from '../../classes/Resume';
import TechnicalSkillEntry from '../../classes/TechnicalSkillEntry';
import TutorialEntry from '../../classes/TutorialEntry';
import WorkExperienceEntry from '../../classes/WorkExperienceEntry';


export interface ILoadingAction {

  type: string;
  isLoading: boolean;

}


export interface IShowErrorAction {

  type: string;
  errorMessage: string;

}


export interface ISetNewsEntriesAction {

  type: string;
  newsEntries: NewsEntry[];

}


export interface ISetEducationEntriesAction {

  type: string;
  educationEntries: EducationEntry[];

}


export interface ISetTechnicalSkillEntriesAction {

  type: string;
  technicalSkillEntries: TechnicalSkillEntry[];

}


export interface ISetWorkExperienceEntriesAction {

  type: string;
  workExperienceEntries: WorkExperienceEntry[];

}


export interface ISetTutorialEntriesAction {

  type: string;
  tutorialEntries: TutorialEntry[];

}


export interface ISetResumeAction {

  type: string;
  resume: Resume;

}


export function showError(errorMessage: string): IShowErrorAction {

    return { type: SET_ERROR, errorMessage };

}


export function showLoading(): ILoadingAction {

  return { type: SET_LOADING, isLoading: true };

}


export function hideLoading(): ILoadingAction {

    return { type: SET_LOADING, isLoading: false };

}


export function setNewsEntries(newsEntries: NewsEntry[]): ISetNewsEntriesAction {

  return { type: SET_NEWS_ENTRIES, newsEntries };

}


export function setEducationEntries(educationEntries: EducationEntry[]): ISetEducationEntriesAction {

  return { type: SET_EDUCATION_ENTRIES, educationEntries };

}


export function setTechnicalSkillEntries(
  technicalSkillEntries: TechnicalSkillEntry[]): ISetTechnicalSkillEntriesAction {

  return { type: SET_TECHNICAL_SKILL_ENTRIES, technicalSkillEntries };

}


export function setWorkExperienceEntries(
  workExperienceEntries: WorkExperienceEntry[]): ISetWorkExperienceEntriesAction {

  return { type: SET_WORK_EXPERIENCE_ENTRIES, workExperienceEntries };

}


export function setTutorialEntries(
  tutorialEntries: TutorialEntry[]): ISetTutorialEntriesAction {

  return { type: SET_TUTORIAL_ENTRIES, tutorialEntries };

}


export function setResume(
  resume: Resume): ISetResumeAction {

  return { type: SET_RESUME, resume };

}

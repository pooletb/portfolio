const proxy = require('http-proxy-middleware');

const DEFAULT_PROXY_ORIGIN_BASE_URL = "https://tylerpoole.dev";

module.exports = function (app) {
  app.use("/api", proxy({
      target: DEFAULT_PROXY_ORIGIN_BASE_URL,
      changeOrigin: true,
  }));
};

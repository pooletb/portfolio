// React
import React from 'react';

// Material UI
import { LinearProgress } from '@material-ui/core';

// Images

// Styles
import './LoadingNavbar.scss';


interface ILoadingNavbarProps {

    isLoading: boolean;

}


const LoadingNavbar = (props: ILoadingNavbarProps) => {

    const { isLoading } = props;

    return (

      <React.Fragment>
        <div className="LoadingNavbar" data-test-name="LoadingNavbar">
          <div className="LoadingNavbar__titleContainer">
            <div className="LoadingNavbar__title" data-test-name="LoadingNavbar__title">T</div>
            <div className="LoadingNavbar__subTitle" data-test-name="LoadingNavbar__subTitle">P</div>
          </div>
          <div className="LoadingNavbar__descriptionContainer">
            <div className="LoadingNavbar__name">
              TYLER POOLE
            </div>
            <div className="LoadingNavbar__position">
              SENIOR FRONT END ENGINEER
            </div>
          </div>
        </div>
        {(isLoading) ?
          <LinearProgress
            className="LoadingNavbar--isLoading"
            data-test-name="LoadingNavbar--isLoading"
          />
          : undefined
        }
      </React.Fragment>

    );

};


export default LoadingNavbar;

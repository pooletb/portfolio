// React
import React, { Component, Dispatch } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

// Redux
import { connect } from 'react-redux';

// Actions
import { openBase64EncodedString } from '../../redux/actions/downloading/openBase64EncodedString';
import { fetchEntries } from '../../redux/actions/networking/fetchEntries';

// Components
import Education from '../Education/Education';
import LoadingNavbar from '../LoadingNavbar/LoadingNavbar';
import Menu from '../Menu/Menu';
import News from '../News/News';
import TechnicalSkills from '../TechnicalSkills/TechnicalSkills';
import Tutorial from '../Tutorial/Tutorial';
import WorkExperience from '../WorkExperience/WorkExperience';

// Classes
import EducationEntry from '../../classes/EducationEntry';
import MainState from '../../classes/MainState';
import NewsEntry from '../../classes/NewsEntry';
import Resume from '../../classes/Resume';
import State from '../../classes/State';
import TechnicalSkillEntry from '../../classes/TechnicalSkillEntry';
import TutorialEntry from '../../classes/TutorialEntry';
import WorkExperienceEntry from '../../classes/WorkExperienceEntry';

// Stylesheets
import './Main.scss';

// Libraries
import {Close as CloseIcon, Menu as MenuIcon} from '@material-ui/icons';


interface IMainProps {

    // State Variables
    isLoading: boolean;
    newsEntries: NewsEntry[];
    educationEntries: EducationEntry[];
    technicalSkillEntries: TechnicalSkillEntry[];
    workExperienceEntries: WorkExperienceEntry[];
    tutorialEntries: TutorialEntry[];
    resume: Resume;

    // Dispatch Functions
    fetchEntries: (
      type: 'NewsEntry' |
      'EducationEntry' |
      'TechnicalSkillEntry' |
      'WorkExperienceEntry' |
      'TutorialEntry' |
      'Resume',
      ) => any;

}


class Main extends Component<IMainProps, MainState> {

    public constructor(props: IMainProps) {

      super(props);
      this.state = new MainState();

    }

    public componentDidMount() {

      this.props.fetchEntries('TutorialEntry');
      document.title = 'Tyler Poole - Front End Engineer';

    }


    public showHideMenu = () => {

      const { showMenu } = this.state;

      this.setState({

        showMenu: !showMenu,

      });

    }


    public routeToNews = () => {

      const { newsEntries } = this.props;

      if (newsEntries.length === 0) {

        this.props.fetchEntries('NewsEntry');

      }

      return <News newsEntries={newsEntries} />;

    }


    public routeToEducation = () => {

      const { educationEntries } = this.props;

      if (educationEntries.length === 0) {

        this.props.fetchEntries('EducationEntry');

      }
      return <Education educationEntries={educationEntries} />;

    }


    public routeToTechnicalSkills = () => {

      const { technicalSkillEntries } = this.props;

      if (technicalSkillEntries.length === 0) {

        this.props.fetchEntries('TechnicalSkillEntry');

      }
      return <TechnicalSkills technicalSkillEntries={technicalSkillEntries} />;

    }


    public routeToWorkExperience = () => {

      const { workExperienceEntries } = this.props;

      if (workExperienceEntries.length === 0) {

        this.props.fetchEntries('WorkExperienceEntry');

      }
      return <WorkExperience workExperienceEntries={workExperienceEntries} />;

    }


    public routeToTutorial = (tutorialEntry: TutorialEntry) => {

      return <Tutorial tutorialEntry={tutorialEntry} />;

    }


    public routeToResume = () => {

      const { resume } = this.props;

      this.props.fetchEntries('Resume');

      if (resume.content !== '') {

        openBase64EncodedString(resume.content, 'PDF', 'Tyler Poole - Senior Front End Engineer');

      }

      return null;

    }


    public render() {

      const {

        isLoading,
        tutorialEntries,

      } = this.props;

      const { showMenu } = this.state;

      return (

        <div className="Main" data-test-name="Main">
          <div className={showMenu ? 'Main__menuShownMobile' : 'Main__menuContainer'}>
            <Menu tutorialEntries={tutorialEntries} />
          </div>
          <div className={showMenu ? 'Main__contentHiddenMobile' : 'Main__blockContainer'} >
            <div className="Main__loadingNavbarContainer">
              <LoadingNavbar isLoading={isLoading} />
              <div className="Main__menuIcon">
                  {
                  showMenu
                  ?
                  <CloseIcon onClick={this.showHideMenu} />
                  :
                  <MenuIcon onClick={this.showHideMenu}/>
                }
              </div>
            </div>
            <div className="Main__contentContainer" >
              <Switch>
                <Route exact={true} path="/" component={this.routeToNews} />
                <Route exact={true} path="/education" component={this.routeToEducation} />
                <Route exact={true} path="/skills" component={this.routeToTechnicalSkills} />
                <Route exact={true} path="/experience" component={this.routeToWorkExperience} />
                <Route exact={true} path="/resume" component={this.routeToResume} />
                {
                  tutorialEntries.length > 0
                  ?
                  tutorialEntries.map((tutorialEntry: TutorialEntry) => (
                    <Route
                      exact={true}
                      path={'/tutorials/' + tutorialEntry.link}
                      component={this.routeToTutorial.bind(undefined, tutorialEntry)}
                      key={tutorialEntry.id}
                    />
                  ))
                  :
                  undefined
                }
                {
                  tutorialEntries.length > 0
                  ?
                  <Redirect to="/" />
                  :
                  undefined
                }
              </Switch>
            </div>
          </div>
        </div>

      );

    }

}


const mapStatetoProps = (state: State) => {

  return {

    isLoading: state.global.isLoading,
    newsEntries: state.global.newsEntries,
    educationEntries: state.global.educationEntries,
    technicalSkillEntries: state.global.technicalSkillEntries,
    workExperienceEntries: state.global.workExperienceEntries,
    tutorialEntries: state.global.tutorialEntries,
    resume: state.global.resume,

  };

};


const mapDispatchToProps = (dispatch: Dispatch<any>) => {

  return {

    fetchEntries: (
      type: 'NewsEntry' |
      'EducationEntry' |
      'TechnicalSkillEntry' |
      'WorkExperienceEntry' |
      'TutorialEntry' |
      'Resume',
      ) => dispatch(fetchEntries(type)),

  };

};


export default connect(mapStatetoProps, mapDispatchToProps) (Main);

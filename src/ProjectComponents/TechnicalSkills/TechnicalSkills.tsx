// React
import React from 'react';

// Classes
import TechnicalSkillEntry from '../../classes/TechnicalSkillEntry';

// Components
import Bubble from '../Bubble/Bubble';

// Stylesheets
import './TechnicalSkills.scss';

// Libraries
import * as icons from 'react-icons/fa';


interface ITechnicalSkillsProps {

  technicalSkillEntries: TechnicalSkillEntry[];

}


const TechnicalSkills = (props: ITechnicalSkillsProps) => {

  const { technicalSkillEntries } = props;

  const hasKey = <O extends {}>(obj: O, key: keyof any): key is keyof O => {

    return key in obj;

  };


  const findIconIfAvailable = (technicalSkillEntry: TechnicalSkillEntry) => {

    const iconString = technicalSkillEntry.icon;

    if (iconString && hasKey(icons, iconString)) {

      const icon = icons[iconString];

      return icon({ size: '5rem'});

    } else {

      return technicalSkillEntry.name;

    }


  };


  return (

  <div className="TechnicalSkills">
    <br />
    <div className="Main__section">
      TECHNICAL SKILLS
    </div>
    <br />
    <div className="Main__divider" />
    <div className="TechnicalSkills__bubbleContainer">
      {
        technicalSkillEntries.length > 0
        ?
        technicalSkillEntries.map((technicalSkillEntry: TechnicalSkillEntry) => (
          <Bubble
            size={technicalSkillEntry.proficiency}
            text={findIconIfAvailable(technicalSkillEntry)}
            key={technicalSkillEntry.id}
          />
        ))
        :
        undefined
      }
    </div>
  </div>

  );

};


export default TechnicalSkills;

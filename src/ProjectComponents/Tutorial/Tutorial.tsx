// React
import React from 'react';

// Classes
import TutorialEntry from '../../classes/TutorialEntry';

// Stylesheets
import './Tutorial.scss';

// Libraries
import AceEditor from 'react-ace';

// Ace
import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/mode-python';
import 'ace-builds/src-noconflict/mode-typescript';
import 'ace-builds/src-noconflict/mode-xml';

import 'ace-builds/src-noconflict/theme-twilight';

interface ITutorialProps {

  tutorialEntry: TutorialEntry;

}


const Tutorial = (props: ITutorialProps) => {

  const { tutorialEntry } = props;

  const evaluatePotentialHTML = (potentialHTMLString: string) => {

    return { __html: potentialHTMLString };

  };


  const constructTutorial = () => {

    const jsxElements = [];

    for (let i = 0; i < tutorialEntry.sections.length; i++) {

      jsxElements.push(
        (
          <React.Fragment>
            <div className="Main__subSection">{tutorialEntry.sections[i]}</div>
            <br />
          </React.Fragment>
        ),
      );

      if (tutorialEntry.images[i]) {

        // Do something

      }

      if (tutorialEntry.snippets[i]) {

        for (const codeSnippet of tutorialEntry.snippets[i]) {

          jsxElements.push(
            (
              <div className="Main__aceEditor">
                <AceEditor
                  placeholder="Looks like theres nothing here."
                  mode={codeSnippet.mode}
                  theme="twilight"
                  name="editor"
                  fontSize={16}
                  showPrintMargin={true}
                  showGutter={true}
                  value={codeSnippet.snippet}
                  setOptions={{
                    showLineNumbers: true,
                    tabSize: 2,
                    readOnly: true,
                    wrap: true,
                  }}
                />
                <br />
              </div>
            ),
          );

        }

      }

      jsxElements.push(
        (
          <React.Fragment>
            <div
              className="Main__paragraph"
              dangerouslySetInnerHTML={evaluatePotentialHTML(tutorialEntry.content[i])}
            />
            <br />
            <br />
          </React.Fragment>
        ),
      );

    }

    return jsxElements;


  };


  return (

    <div className="Tutorial" data-test-name="Tutorial">
      <br />
      <div className="Main__section" dangerouslySetInnerHTML={evaluatePotentialHTML(tutorialEntry.title)}/>
      <br />
      <div className="Main__divider" />
      <br />
      {
        tutorialEntry.sections.length > 0
        ?
        constructTutorial()
        :
        undefined
      }
      <br />
      <br />
    </div>

  );

};


export default Tutorial;

// React
import React from 'react';

// Classes
import WorkExperienceEntry from '../../classes/WorkExperienceEntry';

// Stylesheets
import './WorkExperience.scss';


interface IWorkExperienceProps {

  workExperienceEntries: WorkExperienceEntry[];

}


const WorkExperience = (props: IWorkExperienceProps) => {

  const { workExperienceEntries } = props;

  return (

    <div className="WorkExperience">
        <br />
        <div className="Main__section">WORK EXPERIENCE</div>
        <br />
        <div className="Main__divider" />
        <br />
        {
          workExperienceEntries.length > 0
          ?
          workExperienceEntries.slice(0).reverse().map((workExperienceEntry: WorkExperienceEntry) => (
            <React.Fragment key={workExperienceEntry.id}>
              <div className="Main__subSection">
                {workExperienceEntry.position},
                <span className="Main__highlight"> {workExperienceEntry.organization}</span>
              </div>
              <br />
              <ul className="Main__paragraph">
                {
                  workExperienceEntry.duties.map((duty: string)  => (
                    <React.Fragment key={workExperienceEntry.id + 'duty'}>
                      <li>{duty}</li>
                      <br />
                    </React.Fragment>
                  ))
                }
              </ul>
              <br />
              <br />
            </React.Fragment>

          ))
          :
          undefined
        }

    </div>

  );

};


export default WorkExperience;

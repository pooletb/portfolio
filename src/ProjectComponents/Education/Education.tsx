// React
import React from 'react';

// Classes
import EducationEntry from '../../classes/EducationEntry';

// Stylesheets
import './Education.scss';


interface IEducationProps {

  educationEntries: EducationEntry[];

}


const Education = (props: IEducationProps) => {

  const { educationEntries } = props;

  return (

    <div className="Education">
        <br />
        <div className="Main__section">EDUCATION</div>
        <br />
        <div className="Main__divider" />
        <br />
        {
          educationEntries.length > 0
          ?
          educationEntries.map((educationEntry: EducationEntry) => (
            <React.Fragment key={educationEntry.id}>
              <div className="Main__subSection">
                {educationEntry.degree}
                <span className="Main__highlight"> • {educationEntry.date} • {educationEntry.school}</span>
              </div>
              <br/>
              <br />
            </React.Fragment>
          ))
          :
          undefined
        }
    </div>

  );

};


export default Education;

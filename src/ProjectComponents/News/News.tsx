// React
import React from 'react';

// Classes
import NewsEntry from '../../classes/NewsEntry';

// Stylesheets
import './News.scss';


interface INewsProps {

  newsEntries: NewsEntry[];

}


const News = (props: INewsProps) => {

  const { newsEntries } = props;


  const evaluatePotentialHTML = (potentialHTMLString: string) => {

    return { __html: potentialHTMLString };

  };


  return (

    <div className="Introduction" data-test-name="News">
      <br />
      <div className="Main__section">INTRODUCTION</div>
      <br />
      <div className="Main__divider" />
      <br />
      {
        newsEntries.length > 0
        ?
        newsEntries.map((newsEntry: NewsEntry) => (
          <React.Fragment key={newsEntry.id}>
            <div className="Main__subSection" dangerouslySetInnerHTML={evaluatePotentialHTML(newsEntry.title)}/>
            <br />
            <div className="Main__paragraph" dangerouslySetInnerHTML={evaluatePotentialHTML(newsEntry.content)}/>
            <br/>
            <br />
          </React.Fragment>
        ))
        :
        undefined
      }
    </div>

  );

};


export default News;

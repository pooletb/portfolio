// React
import React from 'react';

// Classes
import TutorialEntry from '../../classes/TutorialEntry';

// Stylesheets
import './Menu.scss';

// Libraries
import { FaDownload,  FaGitlab, FaLinkedin } from 'react-icons/fa';


interface IMenuProps {

  tutorialEntries: TutorialEntry[];

}


const Menu = (props: IMenuProps) => {

  const { tutorialEntries } = props;


  const evaluatePotentialHTML = (potentialHTMLString: string) => {

    return { __html: potentialHTMLString };

  };


  const menuRouter = (routeTo: string) => {

    switch (routeTo) {

      case 'news':
        window.location.href = '/';
        break;

      case 'education':
        window.location.href = '/education';
        break;

      case 'skills':
        window.location.href = '/skills';
        break;

      case 'experience':
        window.location.href = '/experience';
        break;

      case 'gitlab':
        window.location.href = 'https://gitlab.com/pooletb';
        break;

      case 'linkedin':
        window.location.href = 'https://linkedin.com/in/pooletb93';
        break;

      case 'resume':
        window.open('/resume');
        break;

      default:
        window.location.href = routeTo;
        break;

    }

  };


  return (

  <div className="Menu">
    <div className="Menu__titleContainer">
      <div className="Menu__circleContainer">
        <div className="Menu__title">T</div>
        <div className="Menu__subTitle">P</div>
      </div>
    </div>
    <div className="Menu__contentContainer">
      <div onClick={menuRouter.bind(undefined, 'news')} className="Menu__section">
        INTRODUCTION
      </div>
      <div onClick={menuRouter.bind(undefined, 'education')} className="Menu__section">
        EDUCATION
      </div>
      <div onClick={menuRouter.bind(undefined, 'skills')} className="Menu__section">
        TECHNICAL SKILLS
      </div>
      <div onClick={menuRouter.bind(undefined, 'experience')} className="Menu__section">
        WORK EXPERIENCE
      </div>
      <div className="Menu__section">
        TUTORIALS
        {
          tutorialEntries.length > 0
          ?
          tutorialEntries.map((tutorialEntry: TutorialEntry) => (
            <div
              onClick={menuRouter.bind(undefined, 'tutorials/' + tutorialEntry.link)}
              className="Menu__subSection"
              dangerouslySetInnerHTML={evaluatePotentialHTML(tutorialEntry.title)}
              key={tutorialEntry.id}
            />
          ))
          :
          undefined
        }
      </div>
    </div>
    <div className="Menu__linksContainer">
      <div className="Menu__linkIcon">
        <FaGitlab size="1.75rem" onClick={menuRouter.bind(undefined, 'gitlab')} />
      </div>
      <div className="Menu__linkIcon">
        <FaLinkedin size="1.75rem" onClick={menuRouter.bind(undefined, 'linkedin')} />
      </div>
      <div className="Menu__downloadIcon">
        <FaDownload size="1.75rem" onClick={menuRouter.bind(undefined, 'resume')} />
      </div>
    </div>
  </div>

  );

};


export default Menu;

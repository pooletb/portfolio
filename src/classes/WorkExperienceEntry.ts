export default class WorkExperienceEntry {

  public id: string = '';
  public position: string = '';
  public organization: string = '';
  public duties: string[] = [];

}

// Classes
import EducationEntry from './EducationEntry';
import NewsEntry from './NewsEntry';
import Resume from './Resume';
import TechnicalSkillEntry from './TechnicalSkillEntry';
import TutorialEntry from './TutorialEntry';
import WorkExperienceEntry from './WorkExperienceEntry';


export default class GlobalReducerActions {

  public errorMessage: string = '';
  public isLoading: boolean = false;
  public newsEntries: NewsEntry[] = [];
  public educationEntries: EducationEntry[] = [];
  public technicalSkillEntries: TechnicalSkillEntry[] = [];
  public workExperienceEntries: WorkExperienceEntry[] = [];
  public tutorialEntries: TutorialEntry[] = [];
  public resume: Resume = new Resume();
  public type: string = '';

}

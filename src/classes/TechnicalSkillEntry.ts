export default class TechnicalSkillEntry {

  public id: string = '';
  public proficiency: string = '0';
  public name: string = '';
  public icon?: string;

}

export default class NewsEntry {

  public id: string = '';
  public title: string = '';
  public content: string = '';
  public picture?: string;

}

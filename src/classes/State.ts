import GlobalReducerState from '../classes/GlobalReducerState';

export default class State {
  public global: GlobalReducerState = new GlobalReducerState();
}

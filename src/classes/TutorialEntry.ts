// Classes
import CodeSnippet from './CodeSnippet';

export default class TutorialEntry {

  public id: string = '';
  public title: string = '';
  public link: string = '';
  public sections: string[] = [];
  public content: string[] = [];
  public images: string[][] = [];
  public snippets: CodeSnippet[][] = [];

}

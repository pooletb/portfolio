export default class EducationEntry {

  public id: string = '';
  public degree: string = '';
  public date: string = '';
  public school: string = '';

}

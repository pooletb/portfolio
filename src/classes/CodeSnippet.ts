export default class CodeSnippet {

  public mode: 'javascript' |
    'java' |
    'python' |
    'xml' |
    'ruby' |
    'sass' |
    'markdown' |
    'mysql' |
    'json' |
    'html' |
    'handlebars' |
    'golang' |
    'csharp' |
    'elixr' |
    'typescript' |
    'css' = 'typescript';
  public snippet: string = '';

}

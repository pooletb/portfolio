Portfolio
=======
[![pipeline status](https://gitlab.com/pooletb/skeleton-react-typescript/badges/master/pipeline.svg)](https://gitlab.com/pooletb/portfolio/commits/master)

About
=======
A React app that powers my portfolio at https://tylerpoole.dev
